<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Програма перевірки знань зі способів подання графів</title>
<script type="text/javascript" src="startPageJS.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
</head>
<body>
<h1>Програма перевірки знань зі способів подання графів</h1>
<div id = "isOfficial"> 
<p>
	<input type = "button" onClick = "beforeChooseStudent()" 
		value = "Здати офіційно (на бали) усі способи подання свого варіанта">
</p>
<p>
	<input type = "button" onClick = "startChooseNonOfficial()"
		value = "Анонімно перевірити один варіант, один спосіб подання">
</p>
</div>
<div id = "choosesForNonOfficial" hidden = "hidden">
<div id = "variant">
	Варіант: <select name = "chooseVariant" id = "chooseVariant" onChange = "variantSelected()">
	<c:forEach var = "i" begin = "0" end = "15">
		<option>${i}</option>
	</c:forEach>
	</select>
</div>
<div id = "isDirectedDiv">
	<p>Який з двох графів?</p>
	<p><input type = "radio" name = "isDirected" value = "no" checked = "checked">Неорієнтований граф</p>
	<p><input type = "radio" name = "isDirected" value = "yes">Орієнтований граф</p>
</div>
<div id = "whatPresentationDiv">
	<p>Спосіб подання:</p>
	<p><input type = "radio" name = "whatPresentation" value = "incidenceMatrix">Матриця інциденцій</p>
	<p><input type = "radio" name = "whatPresentation" value = "adjMatrix" checked = "checked">Матриця суміжності</p>
	<p><input type = "radio" name = "whatPresentation" value = "adjLists" disabled = "disabled">Списки суміжності</p>
</div>
<button type = "submit" onClick = "endChooseNonOfficial()">Розпочати</button>
</div>
</body>
</html>