function dec_N_rows() {
	if((+document.getElementById("N_rows").value) > 1) {
		document.getElementById("N_rows").value -= 1;
		cellChange('M_cols','N_rows');
	}
}

function inc_N_rows() {
	if((+document.getElementById("N_rows").value) < 17) {
		document.getElementById("N_rows").value -= (-1);
		cellChange('M_cols','N_rows');
	}
}

function dec_M_cols() {
	if((+document.getElementById("M_cols").value) > 1) {
		document.getElementById("M_cols").value -= 1;
		cellChange('N_rows','M_cols');
	}
}

function inc_M_cols() {
	if((+document.getElementById("M_cols").value) < 17) {
		document.getElementById("M_cols").value -= (-1);
		cellChange('N_rows','M_cols');
	}
}

function intToStr(i, j) {
	return "" + j;
}

function inputTagWithIndice(i, j, doFillWithZero) {
	return "<input id = \"matrix-cell-" + i + "-" + j + "\" size = \"1\" value = \"" + (doFillWithZero ? "0" : "") + "\" onChange = \" cellChange('matrix-cell-" + j + "-" + i + "', 'matrix-cell-" + i + "-" + j + "')\">";
}

function AddOrRemoveCells(tbl, i, mmm, funcIdxToHTML, doFillWithZero) {
	if(mmm < tbl.rows[i].cells.length-1) {
		for(var j=tbl.rows[i].cells.length-1; j>mmm; j--) {
			tbl.rows[i].deleteCell(j);
		}
	} 
	else {
		for(var j=tbl.rows[i].cells.length; j<=mmm; j++) {
			if(j >= tbl.rows[i].cells.length) {
				tbl.rows[i].insertCell(j);
				tbl.rows[i].cells[j].innerHTML = funcIdxToHTML(i, j, doFillWithZero);
			}
		}
	}
}

function setOneOfSizes(id, name, max) {
	var xxx = +document.getElementById(id).value;
	if (isNaN(xxx) || xxx < 1) {
		alert("Кількість " + name + "повинна бути натуральним числом");
		return null;
	}
	if(xxx > max) {
		alert("Введено надто велику кількість " + name + xxx + ", зменшено до " + max);
		xxx = 17;
		document.getElementById(id).value = nnn;
	}
	return xxx;
}

function setSizes() {
	var nnn = setOneOfSizes("N_rows", "рядків", 17);
	if(isNaN(nnn)) {
		return;
	}
	var mmm = setOneOfSizes("M_cols", "стовпчиків", 17);
	if(isNaN(mmm)) {
		return;
	}
	document.getElementById("dontReloadWarning").hidden = false;
	if (nnn != mmm) {
		document.getElementById("doSymmetric").style.visibility = "hidden";
	}
	var tbl = document.getElementById("divForMatrixItself").children[0];
	if(nnn+1 < tbl.rows.length   ||   
		tbl.rows.length > 1 && mmm+1 < tbl.rows[0].cells.length) 
	{
		if( ! confirm("Ви впевнені, що хочете видалити деякі з уже існуючих комірок?")) {
 			return;
		}
	}
	AddOrRemoveCells(tbl, 0, mmm, intToStr);
	var doFillWithZero = document.getElementsByName("howInitMatrix")[0].checked;
	if(nnn < tbl.rows.length-1) {
		for(var i=tbl.rows.length-1; i>nnn; i--) {
			tbl.deleteRow(i);
		}
	}
	for(var i=1; i<=nnn; i++) {
		if(i >= tbl.rows.length) {
			tbl.insertRow(i);
			tbl.rows[i].insertCell(0);
			tbl.rows[i].cells[0].innerHTML = "" + i;
		}
		AddOrRemoveCells(tbl, i, mmm, inputTagWithIndice, doFillWithZero);
	}
	document.getElementById("divForMatrixItself").hidden = false;
}

function cellChange(a, b) {
	if(document.getElementById("checkboxDoSymmetric").checked) {
		document.getElementById(a).value = document.getElementById(b).value;
	}
	else {
		document.getElementById("doSymmetric").style.visibility = "hidden";
	}
}

function submitToServer(isOfficial) {
	var tbl = document.getElementById("divForMatrixItself").children[0];
	var presentedByUser = "";
	for(var i=1; i<tbl.rows.length; i++) {
		for(var j=1; j<tbl.rows[i].cells.length; j++) {
			presentedByUser += tbl.rows[i].cells[j].children[0].value + "c";
		}
		presentedByUser += 'rrr';
	}
	$.ajax({
		url: 'theMainCheckStage',
		data: {
			presentedByUser	: presentedByUser			
		},
		success : function(result, status, jqXHR) {
			alert(result.getElementById("mainAns").textContent);
			var ok = result.getElementById("mainAns").textContent == "YES";
			var isOfficial = typeof(result.getElementById("isOfficial")) == 'string';
			if(ok) {
				if(isOfficial) {
					
				}
				else {
					alert("Вітаємо, відповідь правильна");
					location = "index.jsp";
				}
			}
			else {
				if(isOfficial) {
					
				}
				else {
					alert("Відповідь неправильна, пробуйте далі");
				}
			}
		},
		error : function(result, status, jqXHR) {
			alert(result + "\n" + status + "\n" + jqXHR);
		}
	});
}
