<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Здати офіційно (на бали)</title>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</head>
<body>
<table>
<tr>
<td>
<button onClick = "startAuthorizedTest(no, adjMatrix)">Здати подання матрицею суміжності неорієнтованого графа</button>
(Набрано раніше: <c:out value = "${theSelectedStudent.unDirectedAdjMatrScore}"/> балів)
</td>
<td>
<button onClick = "startAuthorizedTest(yes, adjMatrix)">Здати подання матрицею суміжності орієнтованого графа</button>
(Набрано раніше: <c:out value = "${theSelectedStudent.yesDirectedAdjMatrScore}"/> балів)
</td>
</tr>
<tr>
<td>
<button onClick = "startAuthorizedTest(no, IncidenceMatrix)">Здати подання матрицею інциденцій неорієнтованого графа</button>
(Набрано раніше: <c:out value = "${theSelectedStudent.unDirectedIncidenceMatrScore}"/> балів)
</td>
<td>
<button onClick = "startAuthorizedTest(yes, IncidenceMatrix)">Здати подання матрицею інциденцій орієнтованого графа</button>
(Набрано раніше: <c:out value = "${theSelectedStudent.yesDirectedIncidenceMatrScore}"/> балів)
</td>
</tr>
</table>
</body>
</html>

<!-- 
UPDATE `graphchecker`.`student`
SET
`idstudent` = {idstudent: },
`sirName` = {sirName: },
`firstName` = {firstName: },
`grp` = {grp: },
`variant` = {variant: },
`unDirectedAdjMatrScore` = {unDirectedAdjMatrScore: 0},
`unDirectedIncidenceMatrScore` = {unDirectedIncidenceMatrScore: 0},
`unDirectedAdjListsScore` = {unDirectedAdjListsScore: 0},
`yesDirectedAdjMatrScore` = {yesDirectedAdjMatrScore: 0},
`yesDirectedIncidenceMatrScore` = {yesDirectedIncidenceMatrScore: 0},
`yesDirectedAdjListsScore` = {yesDirectedAdjListsScore: 0}
WHERE <{where_condition}>;
 -->