<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Задайте ...</title>
<script type="text/javascript" src="gen.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</head>
<body>
<table>
<tr>
<td>
<c:if test = "${isDirected == 'yes'}">
	<img src = "${graph.directedPictFileName}" >
</c:if>
<c:if test = "${isDirected == 'no'}">
	<img src = "${graph.unDirectedPictFileName}" >
</c:if>
</td>
<td>
<div id = "allSizesEtc">
<div id = "doSymmetric">
<input type = "checkbox" id = "checkboxDoSymmetric" name = "checkboxDoSymmetric">Автоматично підтримувати симетричність матриці
</div>
<div id = "divForSizes">
<table>
<tr>
	<td>Кількість рядків:</td>
	<td><input type = "button" value = "&lt;" onClick = "dec_N_rows()"></td>
	<td><input id = "N_rows" name = "N" size = "3" value = "7" onChange = "cellChange('M_cols','N_rows')"></td>
	<td><input type = "button" value = "&gt;" onClick = "inc_N_rows()"></td>
</tr>
<tr>
	<td>Кількість стовпчиків:</td>
	<td><input type = "button" value = "&lt;" onClick = "dec_M_cols()"></td>
	<td><input id = "M_cols" name = "M" size = "3" value = "7" onChange = "cellChange('N_rows','M_cols')"></td>
	<td><input type = "button" value = "&gt;" onClick = "inc_M_cols()"></td>
</tr>
</table>
<table>
<tr><td>При створенні матриці...</td><td>
<input type = "radio" name = "howInitMatrix" value = "withZeroes" checked = "checked">заповнити комірки нулями
</td></tr>
<tr><td></td><td>
<input type = "radio" name = "howInitMatrix" value = "emptyStrings">лишити комірки порожніми
</td></tr>
</table>
<input type = "button" name="setSizes" value="Встановити розміри" onClick = "setSizes()">
</div>
</div>
</td>
</table>

<div id = "dontReloadWarning" hidden = "true">
<font color = "red">Не намагайтеся перезавантажувати сторінку засобами браузера (F5, тощо) &mdash; це призведе до втрати введених Вами даних
</font>
</div>
<div id = "divForMatrixItself" hidden = "true">
<table>
<tr><td></td></tr>
</table>
<button onClick = "submitToServer()">Здати!</button>
</div>
<p id = "result" hidden = "true">
Тут буде результат
</p>
</body>
</html>