<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Що перевірятимемо?</title>
</head>
<body>
<h1>Програма перевірки знань зі способів подання графів</h1>
<h2>Варіант <%= request.getParameter("chooseVariant") %></h2>
<% 
	String vari = request.getParameter("chooseVariant");
	while(vari.length() < 2) {
		vari = "0" + vari;
	}
	session.setAttribute("variant", vari); 
%>
<form action = "inputMatrix.jsp">
<div id = "isDirectedDiv">
<p>Яке з двох завдань?</p>
<p><input type = "radio" name = "isDirected" value = "no">Неорієнтований граф</p>
<p><input type = "radio" name = "isDirected" value = "yes">Орієнтований граф</p>
</div>
<div id = "whatPresentationDiv">
<p>Спосіб подання:</p>
<p><input type = "radio" name = "whatPresentation" value = "adjMatrix">Матриця суміжності</p>
<p><input type = "radio" name = "whatPresentation" value = "incidenceMatrix">Матриця інциденцій</p>
</div>
<input type = "submit" value = "Розпочати">
</form>
</body>
</html>
