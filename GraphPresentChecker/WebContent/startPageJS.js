function beforeChooseStudent() {
	location = "beforeStudentChoose";
}

function chooseStudent() {
	alert("going to call generateOneTimeCode");
	$("select#selectStudent").attr('disabled', true);
	$("button#selectStudent").attr('disabled', true);
	$.ajax({
		url: 'generateOneTimeCode',
		data: {
			selectedStudent : 1,
			codeName : "theFirstCode",
		},
		success : function(result, status, jqXHR) {
			$("div#enterCode").show();
		},
		error : function(result, status, jqXHR) {
			alert(result + "\n" + status + "\n" + jqXHR);
		}
	});
}

function startChooseNonOfficial() {
	$("div#isOfficial").hide();
	$("div#choosesForNonOfficial").show();
}

function endChooseNonOfficial() {
	var variantValue = $("#chooseVariant").val();
	while(variantValue.length < 2) {
		variantValue = '0' + variantValue;
	}
	$.ajax({
		url: 'setVariantOnly',
		data: {
			variant : variantValue,
			isDirected : $("input[name=isDirected]:checked").val(),
			whatPresentation : $("input[name=whatPresentation]:checked").val()
		},
		success : function(result, status, jqXHR) {
			location = "inputMatrix.jsp";
		},
		error : function(result, status, jqXHR) {
			alert(result + "\n" + status + "\n" + jqXHR);
		}
	});
}

function checkTheCode() {
	location = "checkOneTimeCode?whatCode=theFirstCode&codeItself=" + $("#theCode").val() + "&redirect=authorizedTesting.jsp";
}