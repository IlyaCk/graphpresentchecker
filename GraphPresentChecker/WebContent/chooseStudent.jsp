<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="startPageJS.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<c:if test = "${theSelectedStudent == null}">
<title>Виберіть себе зі списку</title>
</head>
<body>
<div id = "selectStudent">
<select name = "selectStudent" id = "selectStudent">
	<c:forEach var = "theStudent"  items = "${allStudents}">
		<option value = "${theStudent.idstudent}" ><c:out value = "${theStudent.sirName}"/> <c:out value = "${theStudent.firstName}"/> (<c:out value = "${theStudent.grp}"/>) &mdash; варіант <c:out value = "${theStudent.variant}"/></option>
	</c:forEach>
</select> 
<button id = "selectStudent" onClick = "chooseStudent()">Так, це я</button>
</div>
<div id = "enterCode" hidden = "hidden">
Введіть сюди код, який назве викладач:
<input id = "theCode"/>
<button onClick = checkTheCode()>Ввести код</button>
</div>
</c:if>
</body>
</html>