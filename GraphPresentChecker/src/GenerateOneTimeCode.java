

import graphPresentationChecker.beans.Student;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class GenerateOneTimeCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private DriverManagerDataSource dataSource;
	private JdbcTemplate theJdbc;

	public DriverManagerDataSource getDataSource() {
		if(dataSource == null) {
			dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUrl("jdbc:mysql://localhost:3306/graphchecker");
			dataSource.setUsername("user1");
			dataSource.setPassword("password");
			dataSource.getParentLogger().setLevel(Level.ALL);
		}
		return dataSource;
	}

	public JdbcTemplate getJdbc() {
		if(theJdbc==null) {
			theJdbc = new JdbcTemplate(getDataSource(), false);
		}
		return theJdbc;
	}

    public GenerateOneTimeCode() {
        super();
		dataSource = getDataSource();
		theJdbc = getJdbc();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.err.println("beginning to execute GenerateOneTimeCode.doGet()");
		String codeName = null;
		try {
			codeName = request.getParameter("codeName");
			if(codeName == null) {
				throw new NullPointerException();
			}
				
		}
		catch (Exception e) {
			e.printStackTrace();
			codeName = "defaultCode";
		}
		Random r = new Random();		
		int oneTimePassword = 1000 + r.nextInt(9000);
		HttpSession sess = request.getSession(true);
		sess.setAttribute("isOfficial", "official");
		sess.setAttribute(codeName, new Integer(oneTimePassword).toString());
		System.out.println(request.getParameter("selectedStudent"));
		Student theSelectedStudent = theJdbc.queryForObject("SELECT * FROM student WHERE idstudent = " + request.getParameter("selectedStudent"),
				new BeanPropertyRowMapper<Student>(Student.class)				
				);
		sess.setAttribute("student", theSelectedStudent);
		sess.setAttribute("variant", "" + theSelectedStudent.getVariant());
		theJdbc.update("INSERT INTO onetimecode (code, idstudent, codeName) " +
				"VALUES (?, ?, ?)",
				oneTimePassword,
				Integer.parseInt(request.getParameter("selectedStudent")),
				codeName				
				);
		PrintWriter out = response.getWriter();
		out.println("<html><body>code generated successfully</body></html>");
		out.close();
		System.out.println("OneTimeCode " + oneTimePassword + " assigned to student " + theSelectedStudent.getSirName() + " " + theSelectedStudent.getFirstName());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("POST method called, redirecting to GET");
		this.doGet(request, response);
	}
}
