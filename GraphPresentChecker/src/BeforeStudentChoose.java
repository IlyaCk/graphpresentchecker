

import graphPresentationChecker.beans.Student;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Servlet implementation class BeforeStudentChoose
 */
public class BeforeStudentChoose extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private DriverManagerDataSource dataSource;
	private JdbcTemplate theJdbc;

	public DriverManagerDataSource getDataSource() {
		if(dataSource == null) {
			dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUrl("jdbc:mysql://localhost:3306/graphchecker");
			dataSource.setUsername("user1");
			dataSource.setPassword("password");
			dataSource.getParentLogger().setLevel(Level.ALL);
		}
		return dataSource;
	}

	public JdbcTemplate getJdbc() {
		if(theJdbc==null) {
			theJdbc = new JdbcTemplate(getDataSource(), false);
		}
		return theJdbc;
	}

    public BeforeStudentChoose() {
        super();
		dataSource = getDataSource();
		theJdbc = getJdbc();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess = request.getSession(false);
		try {
			sess.invalidate();
		}
		catch (Exception e) {
			;
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sess = request.getSession(true);
		List <Student> allStudents = theJdbc.query("SELECT * FROM student", new BeanPropertyRowMapper<Student>(Student.class));
		System.out.println(allStudents.toString());
		sess.setAttribute("allStudents", allStudents);
		request.getRequestDispatcher("chooseStudent.jsp").forward(request, response);
	}
}
