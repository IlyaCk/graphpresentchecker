

import graphPresentationChecker.beans.Graph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Servlet implementation class SetVariantOnly
 */
public class SetVariantOnly extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DriverManagerDataSource dataSource;
	private JdbcTemplate theJdbc;

	public SetVariantOnly() {
		super();
		dataSource = getDataSource();
		theJdbc = getJdbc();
	}

	public DriverManagerDataSource getDataSource() {
		if(dataSource == null) {
			dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUrl("jdbc:mysql://localhost:3306/graphchecker");
			dataSource.setUsername("user1");
			dataSource.setPassword("password");
			dataSource.getParentLogger().setLevel(Level.ALL);
		}
		return dataSource;
	}

	public JdbcTemplate getJdbc() {
		if(theJdbc==null) {
			theJdbc = new JdbcTemplate(getDataSource(), false);
		}
		return theJdbc;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess = request.getSession();
		if(sess.getAttribute("isOfficial") != null) {
			PrintWriter out = response.getWriter();
			out.println("<html>������ �������������, �� �� ������ ���� ��������� ������</html>");
			out.close();
			return;			
		}
		Integer varAsIntObj = null;
		try {
			varAsIntObj = Integer.parseInt(request.getParameter("variant"));
		} catch ( Exception e ) {
			varAsIntObj = Integer.valueOf(0);
		}
		if(varAsIntObj==null) {
			varAsIntObj = Integer.valueOf(0);
		}
		Graph currGraph = theJdbc.queryForObject(
				"SELECT * FROM graph WHERE variant = ?", 
				new Object[] {varAsIntObj},
				new BeanPropertyRowMapper<Graph>(Graph.class)
				);
		sess.setAttribute("variant", request.getParameter("variant"));
		sess.setAttribute("isDirected", request.getParameter("isDirected"));
		sess.setAttribute("whatPresentation", request.getParameter("whatPresentation"));
		sess.setAttribute("graph", currGraph);
		PrintWriter out = response.getWriter();
		out.println("<html>All before redirect done</html>");
		out.close();
	}
}