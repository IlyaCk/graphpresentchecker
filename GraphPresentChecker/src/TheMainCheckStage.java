import graphPresentationChecker.beans.Graph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class theMainCheckStage
 */
public class TheMainCheckStage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String genRndStr() {
		Random r = new Random();
		StringBuilder s = new StringBuilder(""); 
		for(int k = r.nextInt(10); k>-5; k--) {
			s.append(33 + r.nextInt(200));
		}
		return s.toString();
	}

    private String incidenceToAdjacency(String presentedByUser, boolean isDirected) {
    	String[] byLines = presentedByUser.split("rrr");
    	System.err.println(Arrays.deepToString(byLines));
    	String[][] data = new String[byLines.length][];
    	for(int i=0; i<byLines.length; i++) {
    		data[i] = byLines[i].split("c");
    		if(i>0 && data[i].length != data[i-1].length) {
    			return "Impossible, non-rectangular" + genRndStr();
    		}
    		System.err.println(Arrays.deepToString(data));
    		for(int j=0; j<data[i].length; j++) {
    			if(!("0".equals(data[i][j])  ||  "1".equals(data[i][j])  ||  isDirected && "-1".equals(data[i][j]))) {
    				return "Impossible value(s) in matrix" + genRndStr();
    				// !!! does not support "2" for "loops"
    			}
    		}
    	}

    	System.err.println(presentedByUser);
    	int[][] adjMatrix = new int[byLines.length][byLines.length]; 
    	for(int j=0; j<data[0].length; j++) {
    		Map<String, ArrayList<Integer> > mp = new TreeMap<String, ArrayList<Integer> >();
    		for(int i=0; i<byLines.length; i++) {
    			if(!("0".equals(data[i][j]))) {
        			if(!mp.containsKey(data[i][j])) {
        				mp.put(data[i][j], new ArrayList<Integer>());
        			}
        			mp.get(data[i][j]).add(i);
    			}
    		}
    	
    		if(isDirected) {
    			if(mp.size() != 2 || !mp.containsKey("1") || !mp.containsKey("-1") || mp.get("1").size()!=1 || mp.get("-1").size()!=1) {
    				return "Incorrect column " + (j+1) + " " + genRndStr();
    			}
    			adjMatrix[mp.get("-1").get(0)][mp.get("1").get(0)] += 1;    			    			    			
    		}
    		else { // un-directed
    			if(mp.size() != 1 || !mp.containsKey("1") || mp.get("1").size()!=2) {
    				return "Incorrect column " + (j+1) + " " + genRndStr();
    			}
    			adjMatrix[mp.get("1").get(0)][mp.get("1").get(1)] += 1;    			    			    			
    			adjMatrix[mp.get("1").get(1)][mp.get("1").get(0)] += 1;
    		}
    		System.err.println(Arrays.deepToString(adjMatrix));
    	}
    	StringBuilder mainRes = new StringBuilder("");
    	for(int i=0; i<adjMatrix.length; i++) {
    		for(int j=0; j<adjMatrix[0].length; j++) {
    			mainRes.append("" + adjMatrix[i][j] + "c");
    		}
    		mainRes.append("rrr");
    	}
    	System.err.println(mainRes.toString());
    	return mainRes.toString();
    }
    
    private String adjListsToMatrix(String presentedByUser) {
    	// TODO apply AdjacencyLists -> AdjacencyMatrix transform!!!
    	return presentedByUser;
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		HttpSession sess = request.getSession();
		
		boolean isDirected = !("no".equalsIgnoreCase((String)sess.getAttribute("isDirected")));
		String whatPresentation = (String)sess.getAttribute("whatPresentation");
		String presentedByUser = request.getParameter("presentedByUser");
		Graph currGraph = (Graph)sess.getAttribute("graph");
		if(currGraph==null) {
			System.err.println("currGraph==null");
		}
		if("incidenceMatrix".equalsIgnoreCase(whatPresentation)) {
			presentedByUser = incidenceToAdjacency(presentedByUser, isDirected);
		}
		else if("adjLists".equalsIgnoreCase(whatPresentation)) {
			presentedByUser = adjListsToMatrix(presentedByUser);
		}
		String ethalonePresentation = isDirected ? currGraph.getDirectedEthaloneAnswer() : currGraph.getUnDirectedEthaloneAnswer();
		boolean isOfficial = sess.getAttribute("isOfficial") != null;
		
		out.println("<html><body>\n");
		System.err.println(ethalonePresentation);
		System.err.println(presentedByUser);
		if(ethalonePresentation.equals(presentedByUser)) {
			out.println("<p id = 'mainAns'>YES</p>\n");
			if(isOfficial) {
				out.println("\n<p id = 'isOfficial'>isOfficial</p>\n");
			}
		}
		else {
			out.println("<p id = 'mainAns'>NO</p>\n");
			if(isOfficial) {
				out.println("\n<p id = 'isOfficial'>isOfficial</p>\n");
				String tryType = whatPresentation + isDirected + "tries";
				Object numTries = sess.getAttribute(tryType);
				sess.setAttribute(tryType, Integer.valueOf(numTries == null ? 1 : ((Integer)numTries) +1));
			}
		}
		out.println("</body></html>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
