

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CheckOneTimeCode
 */
public class CheckOneTimeCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckOneTimeCode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("trying to check code");
		String theGotCode = request.getParameter("codeItself");
		String whatCode = request.getParameter("whatCode");
		String redirectIfOk = request.getParameter("redirect");
		HttpSession sess = request.getSession(false);
		if(sess != null  &&  !theGotCode.equals(sess.getAttribute(whatCode))) {
			System.out.println("incorrect code");
			sess.invalidate();
			sess = null;
		}
		if(sess == null) {
			System.out.println("redirecting to start");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		else {
			System.out.println("autorized");
			sess.setAttribute("isOfficial", "official");
			request.getRequestDispatcher(redirectIfOk).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}


