package graphPresentationChecker.beans;

public class Graph {
	int variant;
	String unDirectedPictFileName, directedPictFileName, unDirectedEthaloneAnswer, directedEthaloneAnswer;
	public int getVariant() {
		return variant;
	}
	public void setVariant(int variant) {
		this.variant = variant;
	}
	public String getUnDirectedPictFileName() {
		return unDirectedPictFileName;
	}
	public void setUnDirectedPictFileName(String unDirectedPictFileName) {
		this.unDirectedPictFileName = unDirectedPictFileName;
	}
	public String getDirectedPictFileName() {
		return directedPictFileName;
	}
	public void setDirectedPictFileName(String directedPictFileName) {
		this.directedPictFileName = directedPictFileName;
	}
	public String getUnDirectedEthaloneAnswer() {
		return unDirectedEthaloneAnswer;
	}
	public void setUnDirectedEthaloneAnswer(String unDirectedEthaloneAnswer) {
		this.unDirectedEthaloneAnswer = unDirectedEthaloneAnswer;
	}
	public String getDirectedEthaloneAnswer() {
		return directedEthaloneAnswer;
	}
	public void setDirectedEthaloneAnswer(String directedEthaloneAnswer) {
		this.directedEthaloneAnswer = directedEthaloneAnswer;
	}
}
