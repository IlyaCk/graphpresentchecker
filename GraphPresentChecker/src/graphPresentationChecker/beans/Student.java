package graphPresentationChecker.beans;

import java.io.Serializable;

public class Student implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3954445656613124077L;
	int idstudent;
	String sirName, firstName, grp;
	int variant;
	double unDirectedAdjMatrScore, unDirectedIncidenceMatrScore, unDirectedAdjListsScore,
		yesDirectedAdjMatrScore, yesDirectedIncidenceMatrScore, yesDirectedAdjListsScore;
	public int getIdstudent() {
		return idstudent;
	}
	public void setIdstudent(int idstudent) {
		this.idstudent = idstudent;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSirName() {
		return sirName;
	}
	public void setSirName(String sirName) {
		this.sirName = sirName;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public int getVariant() {
		return variant;
	}
	public void setVariant(int variant) {
		this.variant = variant;
	}
	public double getUnDirectedAdjMatrScore() {
		return unDirectedAdjMatrScore;
	}
	public void setUnDirectedAdjMatrScore(double unDirectedAdjMatrScore) {
		this.unDirectedAdjMatrScore = unDirectedAdjMatrScore;
	}
	public double getUnDirectedIncidenceMatrScore() {
		return unDirectedIncidenceMatrScore;
	}
	public void setUnDirectedIncidenceMatrScore(double unDirectedIncidenceMatrScore) {
		this.unDirectedIncidenceMatrScore = unDirectedIncidenceMatrScore;
	}
	public double getUnDirectedAdjListsScore() {
		return unDirectedAdjListsScore;
	}
	public void setUnDirectedAdjListsScore(double unDirectedAdjListsScore) {
		this.unDirectedAdjListsScore = unDirectedAdjListsScore;
	}
	public double getYesDirectedAdjMatrScore() {
		return yesDirectedAdjMatrScore;
	}
	public void setYesDirectedAdjMatrScore(double yesDirectedAdjMatrScore) {
		this.yesDirectedAdjMatrScore = yesDirectedAdjMatrScore;
	}
	public double getYesDirectedIncidenceMatrScore() {
		return yesDirectedIncidenceMatrScore;
	}
	public void setYesDirectedIncidenceMatrScore(
			double yesDirectedIncidenceMatrScore) {
		this.yesDirectedIncidenceMatrScore = yesDirectedIncidenceMatrScore;
	}
	public double getYesDirectedAdjListsScore() {
		return yesDirectedAdjListsScore;
	}
	public void setYesDirectedAdjListsScore(double yesDirectedAdjListsScore) {
		this.yesDirectedAdjListsScore = yesDirectedAdjListsScore;
	}
}
